<?php

namespace Modules\Website\Http\Controllers;

use Illuminate\Http\Request;

use Modules\Website\Http\Controllers\BaseController;

class HomeController extends BaseController
{
    /**
     * @param nothing
     * @return home page view
     */
    public function index()
    {
        return view('website::home');
    }

    /**
     * @param nothing
     * @return about us page view
     */
    public function about_us()
    {
        return view('website::about-us');
    }

    /**
     * @param nothing
     * @return faq page view
     */
    public function faq()
    {
        return view('website::faq');
    }

    /**
     * @param nothing
     * @return privacy policy page view
     */
    public function privacy_policy()
    {
        return view('website::privacy-policy');
    }

    /**
     * @param nothing
     * @return terms & conditions page view
     */
    public function terms_conditions()
    {
        return view('website::terms-conditions');
    }


}
