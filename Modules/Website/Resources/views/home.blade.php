@extends('website.layouts.app')

@section('page_title')
    {{'Home'}}
@endsection

@section('content')

<!-- home slider section start-->
<div class="">
    <section class="p-0 full-slider shadow-home">
        <div class="slide-1 home-slider home-70">
            <div>
                <div class="home p-left">
                    <img src="./public/frontend-assets/images/home-banner/20.jpg" class="bg-img " alt="">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="slider-contain">
                                    <div>
                                        <h5>all products</h5>
                                        <h1>buy grocery</h1>
                                        <h4>valid till 25 august</h4>
                                        <a href="#" class="btn btn-solid">shop now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="home text-left p-left">
                    <img src="./public/frontend-assets/images/home-banner/19.jpg" class="bg-img " alt="">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="slider-contain">
                                    <div>
                                        <h5>all products</h5>
                                        <h1>buy grocery</h1>
                                        <h4>valid till 25 august</h4>
                                        <a href="#" class="btn btn-solid">shop now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- home slider section end-->


<!-- category section start -->
<section class="category category-classic bg-none absolute-banner">
    <div class="container absolute-bg">
        <div class="slide-6 no-arrow">
            <div>
                <div class="category-wrapper">
                    <div class="img-block">
                        <a href="#"><img src="./public/frontend-assets/images/category/grocery/1.png" alt=""
                                class=" img-fluid"></a>
                    </div>
                    <div class="category-title">
                        <a href="#">
                            <h5>vegetables</h5>
                        </a>
                    </div>
                </div>
            </div>
            <div>
                <div class="category-wrapper">
                    <div class="img-block">
                        <a href="#"><img src="./public/frontend-assets/images/category/grocery/2.png" alt=""
                                class=" img-fluid"></a>
                    </div>
                    <div class="category-title">
                        <a href="#">
                            <h5>foodgrains</h5>
                        </a>
                    </div>
                </div>
            </div>
            <div>
                <div class="category-wrapper">
                    <div class="img-block">
                        <a href="#"><img src="./public/frontend-assets/images/category/grocery/3.png" alt=""
                                class=" img-fluid"></a>
                    </div>
                    <div class="category-title">
                        <a href="#">
                            <h5>bakery</h5>
                        </a>
                    </div>
                </div>
            </div>
            <div>
                <div class="category-wrapper">
                    <div class="img-block">
                        <a href="#"><img src="./public/frontend-assets/images/category/grocery/4.png" alt=""
                                class=" img-fluid"></a>
                    </div>
                    <div class="category-title">
                        <a href="#">
                            <h5>beverage</h5>
                        </a>
                    </div>
                </div>
            </div>
            <div>
                <div class="category-wrapper">
                    <div class="img-block">
                        <a href="#"><img src="./public/frontend-assets/images/category/grocery/5.png" alt=""
                                class=" img-fluid"></a>
                    </div>
                    <div class="category-title">
                        <a href="#">
                            <h5>snacks</h5>
                        </a>
                    </div>
                </div>
            </div>
            <div>
                <div class="category-wrapper">
                    <div class="img-block">
                        <a href="#"><img src="./public/frontend-assets/images/category/grocery/6.png" alt=""
                                class=" img-fluid"></a>
                    </div>
                    <div class="category-title">
                        <a href="#">
                            <h5>kitchen</h5>
                        </a>
                    </div>
                </div>
            </div>
            <div>
                <div class="category-wrapper">
                    <div class="img-block">
                        <a href="#"><img src="./public/frontend-assets/images/category/grocery/7.png" alt=""
                                class=" img-fluid"></a>
                    </div>
                    <div class="category-title">
                        <a href="#">
                            <h5>beauty</h5>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- category section end -->


<!-- collection banner start -->
<section class="ratio2_1 section-b-space">
    <div class="container">
        <div class="row partition-3">
            <div class="col-md-6">
                <a href="#">
                    <div class="collection-banner border-0 p-center text-center">
                        <div class="img-part">
                            <img src="./public/frontend-assets/images/grocery/banner/4.jpg" class=" img-fluid  bg-img"
                                alt="">
                        </div>
                        <div class="contain-banner">
                            <div>
                                <div class="banner-deal">
                                    <h6>free shipping</h6>
                                </div>
                                <h3>nikon camera</h3>
                                <h6>shop now</h6>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="#">
                    <div class="collection-banner border-0 p-center text-center">
                        <div class="img-part">
                            <img src="./public/frontend-assets/images/grocery/banner/5.jpg" class=" img-fluid bg-img"
                                alt="">
                        </div>
                        <div class="contain-banner">
                            <div>
                                <div class="banner-deal">
                                    <h6>minimum 30% off</h6>
                                </div>
                                <h3>kids fashion</h3>
                                <h6>shop now</h6>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- collection banner end -->


<!-- tab section start -->
<section class=" tab-layout1 section-b-space">
    <div class="theme-tab">
        <ul class="tabs border-top-0">
            <li class="current">
                <a href="tab-1.html">top rated</a>
            </li>
            <li class="">
                <a href="tab-2.html">on sale</a>
            </li>
            <li class="">
                <a href="tab-3.html">30% off</a>
            </li>
            <li class="">
                <a href="tab-4.html">most popular</a>
            </li>
            <li class="">
                <a href="tab-5.html">popular</a>
            </li>
        </ul>
        <div class="tab-content-cls ratio_square">
            <div id="tab-1" class="tab-content active default">
                <div class="container shadow-cls">
                    <div class="drop-shadow">
                        <div class="left-shadow">
                            <img src="./public/frontend-assets/images/left.png" alt="" class=" img-fluid">
                        </div>
                        <div class="right-shadow">
                            <img src="./public/frontend-assets/images/right.png" alt="" class=" img-fluid">
                        </div>
                    </div>
                    <div class="row border-row1 m-0">
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/1.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/2.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/3.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/4.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/5.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/6.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/7.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/8.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/9.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/10.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/1.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/2.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-2" class="tab-content">
                <div class="container shadow-cls">
                    <div class="drop-shadow">
                        <div class="left-shadow">
                            <img src="./public/frontend-assets/images/left.png" alt="" class=" img-fluid">
                        </div>
                        <div class="right-shadow">
                            <img src="./public/frontend-assets/images/right.png" alt="" class=" img-fluid">
                        </div>
                    </div>
                    <div class="row border-row1">
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/1.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/2.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/3.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/4.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/5.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/6.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/7.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/8.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/9.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/10.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/1.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/2.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-3" class="tab-content">
                <div class="container shadow-cls">
                    <div class="drop-shadow">
                        <div class="left-shadow">
                            <img src="./public/frontend-assets/images/left.png" alt="" class=" img-fluid">
                        </div>
                        <div class="right-shadow">
                            <img src="./public/frontend-assets/images/right.png" alt="" class=" img-fluid">
                        </div>
                    </div>
                    <div class="row border-row1">
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/1.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/2.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/3.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/4.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/5.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/6.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/7.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/8.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/9.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/10.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/1.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/2.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-4" class="tab-content">
                <div class="container shadow-cls">
                    <div class="drop-shadow">
                        <div class="left-shadow">
                            <img src="./public/frontend-assets/images/left.png" alt="" class=" img-fluid">
                        </div>
                        <div class="right-shadow">
                            <img src="./public/frontend-assets/images/right.png" alt="" class=" img-fluid">
                        </div>
                    </div>
                    <div class="row border-row1">
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/1.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/2.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/3.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/4.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/5.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/6.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/7.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/8.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/9.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/10.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/1.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/2.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-5" class="tab-content">
                <div class="container shadow-cls">
                    <div class="drop-shadow">
                        <div class="left-shadow">
                            <img src="./public/frontend-assets/images/left.png" alt="" class=" img-fluid">
                        </div>
                        <div class="right-shadow">
                            <img src="./public/frontend-assets/images/right.png" alt="" class=" img-fluid">
                        </div>
                    </div>
                    <div class="row border-row1">
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/1.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/2.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/3.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/4.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/5.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/6.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/7.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/8.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/9.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/10.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/1.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-4 col-6 p-0">
                            <div class="product-box product-select-box">
                                <div class="img-block">
                                    <a href="#"><img src="./public/frontend-assets/images/grocery/pro/2.jpg"
                                            class=" img-fluid bg-img" alt=""></a>
                                    <div class="cart-details">
                                        <button title="Add to Wishlist"><i class="ti-heart"
                                                aria-hidden="true"></i></button>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                            title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                        <a href="compare.html" title="Compare"><i class="ti-reload"
                                                aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <a href="#">
                                        <h6>richard mcClintock</h6>
                                    </a>
                                    <h5>$963.00</h5>
                                </div>
                                <div class="select-dropdown">
                                    <select>
                                        <option value="500">500 g - $8</option>
                                        <option value="250">250 g - $5</option>
                                        <option value="100">100 g - $3</option>
                                    </select>
                                </div>
                                <div class="qty-add-box">
                                    <div class="qty-box">
                                        <label>qty:</label>
                                        <div class="input-group">
                                            <input type="number" name="quantity" class="form-control input-number"
                                                value="1">
                                        </div>
                                        <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- tab section start -->


<!-- Category banner section start -->
<section class="ratio2_1">
    <div class="container">
        <div class="row partition-3">
            <div class="col-md-4">
                <a href="#">
                    <div class="collection-banner p-right text-center">
                        <div class="img-part">
                            <img src="./public/frontend-assets/images/grocery/banner/1.jpg" class=" img-fluid bg-img"
                                alt="">
                        </div>
                        <div class="contain-banner banner-3">
                            <div>
                                <div class="banner-deal">
                                    <h6>save 10% off</h6>
                                </div>
                                <h3>leather bag</h3>
                                <h6>shop now</h6>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="#">
                    <div class="collection-banner p-left">
                        <div class="img-part">
                            <img src="./public/frontend-assets/images/grocery/banner/2.jpg" class=" img-fluid bg-img"
                                alt="">
                        </div>
                        <div class="contain-banner banner-3">
                            <div>
                                <div class="banner-deal">
                                    <h6>-50% off</h6>
                                </div>
                                <h3>sports shoes</h3>
                                <h6>shop now</h6>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="#">
                    <div class="collection-banner p-right text-center">
                        <div class="img-part">
                            <img src="./public/frontend-assets/images/grocery/banner/3.jpg" class=" img-fluid bg-img"
                                alt="">
                        </div>
                        <div class="contain-banner banner-3">
                            <div>
                                <div class="banner-deal">
                                    <h6>free shipping</h6>
                                </div>
                                <h3>mac book</h3>
                                <h6>shop now</h6>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- Category banner section end -->


<!-- Product slider section start -->
<section class="slider-section slider-layout-4 section-b-space">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-6 pr-0 ratio_square line-abjust">
                <div class="tab-head">
                    <h2 class="title">last chance to buy</h2>
                </div>
                <div class="veg-2">
                    <div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/1.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/2.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/3.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/4.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/5.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/6.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/7.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/8.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 side-banner side-banner3 pr-0 d-none d-xl-block">
                <a href="#"><img src="./public/frontend-assets/images/grocery/1.jpg" alt="" class=" img-fluid "></a>
            </div>
            <div class="col-xl-6 col-lg-6 pl-0 ratio_square same-slider">
                <div class="tab-head">
                    <h2 class="title">refurbished Products</h2>
                </div>
                <div class="veg-3">
                    <div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/10.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/9.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/8.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/7.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/6.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/5.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/4.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="product-box product-select-box">
                            <div class="img-block">
                                <a href="#"><img src="./public/frontend-assets/images/grocery/pro/3.jpg"
                                        class=" img-fluid bg-img" alt=""></a>
                                <div class="cart-details">
                                    <button title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view"
                                        title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                    <a href="compare.html" title="Compare"><i class="ti-reload"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#">
                                    <h6>richard mcClintock</h6>
                                </a>
                                <h5>$963.00</h5>
                            </div>
                            <div class="select-dropdown">
                                <select>
                                    <option value="500">500 g - $8</option>
                                    <option value="250">250 g - $5</option>
                                    <option value="100">100 g - $3</option>
                                </select>
                            </div>
                            <div class="qty-add-box">
                                <div class="qty-box">
                                    <label>qty:</label>
                                    <div class="input-group">
                                        <input type="number" name="quantity" class="form-control input-number"
                                            value="1">
                                    </div>
                                    <button title="buy now">add <i class="ti-shopping-cart"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Product slider section end -->


<!-- blog section start -->
<section class="blog-section grey-bg section-b-space ratio3_2">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <h2 class="title">from the blog</h2>
                <div class="slide-3">
                    <div>
                        <a href="#">
                            <div class="blog-image">
                                <img src="./public/frontend-assets/images/grocery/blog/2.jpg" class=" img-fluid bg-img"
                                    alt="">
                            </div>
                            <div class="blog-info">
                                <div>
                                    <h5>25 july 2018</h5>
                                    <p>Sometimes on purpose ected humour. dummy text.</p>
                                    <h6>by: admin, 0 comment</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <div class="blog-image">
                                <img src="./public/frontend-assets/images/grocery/blog/1.jpg" class=" img-fluid bg-img"
                                    alt="">
                            </div>
                            <div class="blog-info">
                                <div>
                                    <h5>25 july 2018</h5>
                                    <p>Sometimes on purpose ected humour. dummy text.</p>
                                    <h6>by: admin, 0 comment</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <div class="blog-image">
                                <img src="./public/frontend-assets/images/grocery/blog/2.jpg" class=" img-fluid bg-img"
                                    alt="">
                            </div>
                            <div class="blog-info">
                                <div>
                                    <h5>25 july 2018</h5>
                                    <p>Sometimes on purpose ected humour. dummy text.</p>
                                    <h6>by: admin, 0 comment</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <div class="blog-image">
                                <img src="./public/frontend-assets/images/grocery/blog/3.jpg" class=" img-fluid bg-img"
                                    alt="">
                            </div>
                            <div class="blog-info">
                                <div>
                                    <h5>25 july 2018</h5>
                                    <p>Sometimes on purpose ected humour. dummy text.</p>
                                    <h6>by: admin, 0 comment</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <div class="blog-image">
                                <img src="./public/frontend-assets/images/grocery/blog/4.jpg" class=" img-fluid bg-img"
                                    alt="">
                            </div>
                            <div class="blog-info">
                                <div>
                                    <h5>25 july 2018</h5>
                                    <p>Sometimes on purpose ected humour. dummy text.</p>
                                    <h6>by: admin, 0 comment</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="review-box">
                    <h2 class="title">satisfied customer</h2>
                    <div class="slide-1">
                        <div>
                            <div class="review-content">
                                <div class="avtar">
                                    <img src="./public/frontend-assets/images/testimonial/1.jpg" alt="">
                                </div>
                                <h5>mark jecno</h5>
                                <h6>designer</h6>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have
                                    suffered alteration dummy text .</p>
                            </div>
                        </div>
                        <div>
                            <div class="review-content">
                                <div class="avtar">
                                    <img src="./public/frontend-assets/images/testimonial/1.jpg" alt="">
                                </div>
                                <h5>mark jecno</h5>
                                <h6>designer</h6>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have
                                    suffered alteration dummy text .</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- blog section end -->


<!-- section start -->
<section class="">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 logo no-border">
                <h2 class="title">trusted brand</h2>
                <div class="logo-3 border-logo">
                    <div>
                        <div class="logo-img">
                            <img src="./public/frontend-assets/images/logo/1.png" class=" img-fluid" alt="">
                        </div>
                        <div class="logo-img">
                            <img src="./public/frontend-assets/images/logo/2.png" class=" img-fluid" alt="">
                        </div>
                    </div>
                    <div>
                        <div class="logo-img">
                            <img src="./public/frontend-assets/images/logo/3.png" class=" img-fluid" alt="">
                        </div>
                        <div class="logo-img">
                            <img src="./public/frontend-assets/images/logo/4.png" class=" img-fluid" alt="">
                        </div>
                    </div>
                    <div>
                        <div class="logo-img">
                            <img src="./public/frontend-assets/images/logo/5.png" class=" img-fluid" alt="">
                        </div>
                        <div class="logo-img">
                            <img src="./public/frontend-assets/images/logo/6.png" class=" img-fluid" alt="">
                        </div>
                    </div>
                    <div>
                        <div class="logo-img">
                            <img src="./public/frontend-assets/images/logo/7.png" class=" img-fluid" alt="">
                        </div>
                        <div class="logo-img">
                            <img src="./public/frontend-assets/images/logo/8.png" class=" img-fluid" alt="">
                        </div>
                    </div>
                    <div>
                        <div class="logo-img">
                            <img src="./public/frontend-assets/images/logo/2.png" class=" img-fluid" alt="">
                        </div>
                        <div class="logo-img">
                            <img src="./public/frontend-assets/images/logo/3.png" class=" img-fluid" alt="">
                        </div>
                    </div>
                    <div>
                        <div class="logo-img">
                            <img src="./public/frontend-assets/images/logo/2.png" class=" img-fluid" alt="">
                        </div>
                        <div class="logo-img">
                            <img src="./public/frontend-assets/images/logo/3.png" class=" img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 offset-xl-1">
                <div class="app-section">
                    <div class="img-block">
                        <img src="./public/frontend-assets/images/app/3.jpg" class=" img-fluid" alt="">
                    </div>
                    <div class="app-content">
                        <div>
                            <h5>download the big market app</h5>
                            <div class="app-buttons">
                                <a href="#"><img src="./public/frontend-assets/images/app/app-storw.png"
                                        class=" img-fluid" alt=""></a>
                                <a href="#"><img src="./public/frontend-assets/images/app/play-store.png"
                                        class=" img-fluid" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section end -->

@endsection
