<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('about-us', 'HomeController@about_us');
Route::get('faq', 'HomeController@faq');
Route::get('privacy-policy', 'HomeController@privacy_policy');
Route::get('terms-conditions', 'HomeController@terms_conditions');


