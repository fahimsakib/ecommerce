<!--footer start -->
<footer class="footer-3">
    <div class="subscribe-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="subscribe-content">
                        <h4> <i class="fa fa-envelope-o" aria-hidden="true"></i>newsletter</h4>
                        <p>If you are going to use a passage of Lorem you need. </p>
                        <form class="form-inline subscribe-form">
                            <div class="form-group mb-0">
                                <input type="text" class="form-control" id="exampleFormControlInput3"
                                    placeholder="Email...">
                            </div>
                            <button type="submit" class="btn btn-solid">subscribe</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="instagram section-b-space">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="instagram-banner">
                        <h5>follow us <span>#big market</span></h5>
                    </div>
                    <div class="slide-10 no-arrow slick-instagram">
                        <div>
                            <a href="#">
                                <div class="instagram-box">
                                    <img src="./public/frontend-assets/images/grocery/insta/1.jpg" alt=""
                                        class=" img-fluid">
                                    <div class="overlay">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <div class="instagram-box">
                                    <img src="./public/frontend-assets/images/grocery/insta/2.jpg" alt=""
                                        class=" img-fluid">
                                    <div class="overlay">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <div class="instagram-box">
                                    <img src="./public/frontend-assets/images/grocery/insta/3.jpg" alt=""
                                        class=" img-fluid">
                                    <div class="overlay">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <div class="instagram-box">
                                    <img src="./public/frontend-assets/images/grocery/insta/4.jpg" alt=""
                                        class=" img-fluid">
                                    <div class="overlay">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <div class="instagram-box">
                                    <img src="./public/frontend-assets/images/grocery/insta/5.jpg" alt=""
                                        class=" img-fluid">
                                    <div class="overlay">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <div class="instagram-box">
                                    <img src="./public/frontend-assets/images/grocery/insta/6.jpg" alt=""
                                        class=" img-fluid">
                                    <div class="overlay">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <div class="instagram-box">
                                    <img src="./public/frontend-assets/images/grocery/insta/7.jpg" alt=""
                                        class=" img-fluid">
                                    <div class="overlay">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <div class="instagram-box">
                                    <img src="./public/frontend-assets/images/grocery/insta/8.jpg" alt=""
                                        class=" img-fluid">
                                    <div class="overlay">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <div class="instagram-box">
                                    <img src="./public/frontend-assets/images/grocery/insta/9.jpg" alt=""
                                        class=" img-fluid">
                                    <div class="overlay">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <div class="instagram-box">
                                    <img src="./public/frontend-assets/images/grocery/insta/10.jpg" alt=""
                                        class=" img-fluid">
                                    <div class="overlay">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <div class="instagram-box">
                                    <img src="./public/frontend-assets/images/grocery/insta/2.jpg" alt=""
                                        class=" img-fluid">
                                    <div class="overlay">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <div class="instagram-box">
                                    <img src="./public/frontend-assets/images/grocery/insta/4.jpg" alt=""
                                        class=" img-fluid">
                                    <div class="overlay">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="footer-section">
        <div class="container">
            <div class="row border-cls section-b-space section-t-space">
                <div class="col-xl-4 col-lg-12 about-section">
                    <div class="footer-title footer-mobile-title">
                        <h4>about</h4>
                    </div>
                    <div class="footer-content">
                        <div class="footer-logo">
                            <img src="./public/frontend-assets/images/icon/logo3.png" alt="">
                        </div>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a
                            page.</p>
                        <div class="footer-social">
                            <ul>
                                <li>
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-12">
                    <div class="row height-cls">
                        <div class="col-lg-3 footer-link">
                            <div>
                                <div class="footer-title">
                                    <h4>my account</h4>
                                </div>
                                <div class="footer-content">
                                    <ul>
                                        <li><a href="#">about us</a></li>
                                        <li><a href="#">contact us</a></li>
                                        <li><a href="#">terms & conditions</a></li>
                                        <li><a href="#">return & exchanges</a></li>
                                        <li><a href="#">secure shopping</a></li>
                                        <li><a href="#">shipping & delivery</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 footer-link">
                            <div>
                                <div class="footer-title">
                                    <h4>quick link</h4>
                                </div>
                                <div class="footer-content">
                                    <ul>
                                        <li><a href="#">store location</a></li>
                                        <li><a href="#">my account</a></li>
                                        <li><a href="#">order tracking</a></li>
                                        <li><a href="#">size guide</a></li>
                                        <li><a href="#">my cart</a></li>
                                        <li><a href="#">FAQ</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 footer-link">
                            <div>
                                <div class="footer-title">
                                    <h4>category</h4>
                                </div>
                                <div class="footer-content">
                                    <ul>
                                        <li><a href="#">fashion</a></li>
                                        <li><a href="#">shoes</a></li>
                                        <li><a href="#">cosmetic</a></li>
                                        <li><a href="#">mobile</a></li>
                                        <li><a href="#">electronics</a></li>
                                        <li><a href="#">bags</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 footer-link">
                            <div>
                                <div class="footer-title">
                                    <h4>contact us</h4>
                                </div>
                                <div class="footer-content">
                                    <ul class="contact-list">
                                        <li><i class="fa fa-map-marker"></i>Fiot Fashion Demo Store
                                            India-3654123</li>
                                        <li><i class="fa fa-phone"></i>Call Us: 123-456-7898</li>
                                        <li><i class="fa fa-envelope-o"></i>Email Us: Support@Fiot.com</li>
                                        <li><i class="fa fa-fax"></i>Fax: 123456</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="footer-bottom">
                        <ul>
                            <li><a href="#">mens</a></li>
                            <li><a href="#">womens</a></li>
                            <li><a href="#">clothing</a></li>
                            <li><a href="#">accessories</a></li>
                            <li><a href="#">featured</a></li>
                            <li><a href="#">service</a></li>
                            <li><a href="#">cart</a></li>
                            <li><a href="#">my order</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12">
                    <div class="footer-end">
                        <p><i class="fa fa-copyright" aria-hidden="true"></i> 2018-19 themeforest powered by pixelstrap
                        </p>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12">
                    <div class="payment-card-bottom">
                        <ul>
                            <li>
                                <a href="#"><img src="./public/frontend-assets/images/icon/visa.png" alt=""></a>
                            </li>
                            <li>
                                <a href="#"><img src="./public/frontend-assets/images/icon/mastercard.png" alt=""></a>
                            </li>
                            <li>
                                <a href="#"><img src="./public/frontend-assets/images/icon/paypal.png" alt=""></a>
                            </li>
                            <li>
                                <a href="#"><img src="./public/frontend-assets/images/icon/american-express.png"
                                        alt=""></a>
                            </li>
                            <li>
                                <a href="#"><img src="./public/frontend-assets/images/icon/discover.png" alt=""></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer end -->
